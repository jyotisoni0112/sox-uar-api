package com.sox.uar;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication
@EnableDiscoveryClient
public class UserAccessReviewApplication {

	public static void main(String[] args) {
		SpringApplication.run(UserAccessReviewApplication.class, args);
	}

}
