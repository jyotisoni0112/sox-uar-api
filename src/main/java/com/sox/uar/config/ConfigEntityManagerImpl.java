package com.sox.uar.config;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

@Configuration
@EnableJpaRepositories(
        entityManagerFactoryRef = "soxEntityManagerFactory",
        transactionManagerRef = "soxTransactionManager",
        basePackages = "com.sox.uar.repository"
)
@EnableTransactionManagement
public class ConfigEntityManagerImpl implements ConfigEntityManager{

    @Override
    @Bean(name = "soxEntityManagerFactory")
    public LocalContainerEntityManagerFactoryBean entityManagerFactory(EntityManagerFactoryBuilder builder,
                                                                       final @Qualifier("soxDatasource") DataSource datasource) {
        return builder
                .dataSource(datasource)
                .packages("com.sox.uar.entity")
                .build();
    }

    @Bean(name = "soxTransactionManager")
    public PlatformTransactionManager transactionManager(@Qualifier("soxEntityManagerFactory") EntityManagerFactory entityManager) {
        return new JpaTransactionManager(entityManager);
    }

}
