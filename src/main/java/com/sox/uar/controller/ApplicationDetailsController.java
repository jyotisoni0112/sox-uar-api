package com.sox.uar.controller;

import com.sox.uar.entity.ApplicationEntity;
import com.sox.uar.exception.SOXException;
import com.sox.uar.payload.ApplicationRequest;
import com.sox.uar.payload.SOXResponse;
import com.sox.uar.service.ApplicationService;
import com.sox.uar.utility.SOXConstants;
import com.sox.uar.vo.ErrorDetails;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/application")
@Log4j2
public class ApplicationDetailsController {

    String className = ApplicationDetailsController.class.getName();

    @Autowired
    private ApplicationService applicationService;

    @GetMapping("get-all")
    @PreAuthorize("hasAuthority('ADMIN')")
    public SOXResponse<List<ApplicationEntity>> getAllApplication(){
        String methodName = "getAllApplication";
        SOXResponse<List<ApplicationEntity>> soxResponse = new SOXResponse<>();
        try{
            List<ApplicationEntity> applicationEntityList = applicationService.getAllApplicationDetails();
            soxResponse.setData(applicationEntityList);
            soxResponse.setStatusCode(SOXConstants.STATUS_CODE_SUCCESS);
            soxResponse.setStatusMsg(SOXConstants.STATUS_MSG_SUCCESS);
        }catch (SOXException exception){
            ErrorDetails errorDetails = new ErrorDetails(exception.getErrorMsg(), exception.getStatusCode(), exception.getStatusMsg()
                    , exception.getClassName(), exception.getMethodName(), SOXConstants.NAMESPACE);
            soxResponse.setErrorDetails(errorDetails);
        }catch (Exception exception){
            log.error(exception);
            ErrorDetails errorDetails = new ErrorDetails(exception.getMessage(), SOXConstants.STATUS_CODE_APPLICATION_ERROR, SOXConstants.STATUS_MSG_APPLICATION_ERROR
                                        , className, methodName, SOXConstants.NAMESPACE);
            soxResponse.setErrorDetails(errorDetails);
        }
        return soxResponse;
    }

    @GetMapping("toggle/{appId}")
    @PreAuthorize("hasAuthority('ADMIN')")
    public SOXResponse<ApplicationEntity> toggleApplicationStatus(@PathVariable String appId){
        String methodName = "toggleApplicationStatus";
        SOXResponse<ApplicationEntity> soxResponse = new SOXResponse<>();
        try{
            String userId = SecurityContextHolder.getContext().getAuthentication().getName();
            if(userId.isEmpty()){
                throw new SOXException("UserId is empty. ", SOXConstants.STATUS_CODE_NOT_FOUND
                        , SOXConstants.STATUS_MSG_NOT_FOUND, className, methodName);
            }
            ApplicationEntity applicationEntity = applicationService.toggleApplicationStatus(appId, userId);
            soxResponse.setData(applicationEntity);
            soxResponse.setStatusCode(SOXConstants.STATUS_CODE_SUCCESS);
            soxResponse.setStatusMsg(SOXConstants.STATUS_MSG_SUCCESS);
        }catch (SOXException exception){
            ErrorDetails errorDetails = new ErrorDetails(exception.getErrorMsg(), exception.getStatusCode(), exception.getStatusMsg()
                    , exception.getClassName(), exception.getMethodName(), SOXConstants.NAMESPACE);
            soxResponse.setErrorDetails(errorDetails);
        }catch (Exception exception){
            log.error(exception);
            ErrorDetails errorDetails = new ErrorDetails(exception.getMessage(), SOXConstants.STATUS_CODE_APPLICATION_ERROR, SOXConstants.STATUS_MSG_APPLICATION_ERROR
                    , className, methodName, SOXConstants.NAMESPACE);
            soxResponse.setErrorDetails(errorDetails);
        }
        return soxResponse;
    }

    @PostMapping("add")
    @PreAuthorize("hasAuthority('ADMIN')")
    public SOXResponse<ApplicationEntity> addNewApplication(@RequestBody ApplicationRequest request){
        String methodName = "toggleApplicationStatus";
        SOXResponse<ApplicationEntity> soxResponse = new SOXResponse<>();
        try{
            String userId = SecurityContextHolder.getContext().getAuthentication().getName();
            if(userId.isEmpty()){
                throw new SOXException("UserId is empty. ", SOXConstants.STATUS_CODE_NOT_FOUND
                        , SOXConstants.STATUS_MSG_NOT_FOUND, className, methodName);
            }
            if(request == null){
                throw new SOXException("Invalid Request Body", SOXConstants.STATUS_CODE_VALIDATION_ERROR
                        , SOXConstants.STATUS_MSG_VALIDATION_ERROR, className, methodName);
            }
            if(request.getAppId().isEmpty() || request.getAppName().isEmpty()){
                throw new SOXException("Invalid Request Body", SOXConstants.STATUS_CODE_VALIDATION_ERROR
                        , SOXConstants.STATUS_MSG_VALIDATION_ERROR, className, methodName);
            }
            ApplicationEntity applicationEntity = applicationService.addNewApplication(request, userId);
            soxResponse.setData(applicationEntity);
            soxResponse.setStatusCode(SOXConstants.STATUS_CODE_SUCCESS);
            soxResponse.setStatusMsg(SOXConstants.STATUS_MSG_SUCCESS);
        }catch (SOXException exception){
            ErrorDetails errorDetails = new ErrorDetails(exception.getErrorMsg(), exception.getStatusCode(), exception.getStatusMsg()
                    , exception.getClassName(), exception.getMethodName(), SOXConstants.NAMESPACE);
            soxResponse.setErrorDetails(errorDetails);
        }catch (Exception exception){
            log.error(exception);
            ErrorDetails errorDetails = new ErrorDetails(exception.getMessage(), SOXConstants.STATUS_CODE_APPLICATION_ERROR, SOXConstants.STATUS_MSG_APPLICATION_ERROR
                    , className, methodName, SOXConstants.NAMESPACE);
            soxResponse.setErrorDetails(errorDetails);
        }
        return soxResponse;
    }

}
