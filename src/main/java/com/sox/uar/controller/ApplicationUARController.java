package com.sox.uar.controller;

import com.sox.uar.entity.ApplicationUAREntity;
import com.sox.uar.exception.SOXException;
import com.sox.uar.payload.SOXResponse;
import com.sox.uar.service.LegacyUARService;
import com.sox.uar.utility.SOXConstants;
import com.sox.uar.vo.ErrorDetails;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/legacy")
@Log4j2
public class ApplicationUARController {

    String className = ApplicationUARController.class.getName();

    @Autowired
    private LegacyUARService legacyUARService;

    @GetMapping("/get-uar-data/{appId}")
    public SOXResponse<List<ApplicationUAREntity>> getUserAccessReviewDataPending(@PathVariable String appId){
        String methodName = "getUserAccessReviewDataPending";
        SOXResponse<List<ApplicationUAREntity>> soxResponse = new SOXResponse<>();
        try{
            List<ApplicationUAREntity> entityList = legacyUARService.getUserAccessReviewData(appId, SOXConstants.UAR_STATUS_PENDING);
            soxResponse.setData(entityList);
            soxResponse.setStatusCode(SOXConstants.STATUS_CODE_SUCCESS);
            soxResponse.setStatusMsg(SOXConstants.STATUS_MSG_SUCCESS);
        }catch (SOXException exception){
            ErrorDetails errorDetails = new ErrorDetails(exception.getErrorMsg(), exception.getStatusCode(), exception.getStatusMsg()
                    ,exception.getClassName(), exception.getMethodName(), SOXConstants.NAMESPACE);
            soxResponse.setErrorDetails(errorDetails);
        }catch (Exception exception){
            log.error(exception);
            ErrorDetails errorDetails = new ErrorDetails(exception.getMessage(), SOXConstants.STATUS_CODE_APPLICATION_ERROR, SOXConstants.STATUS_MSG_APPLICATION_ERROR
                    , className, methodName, SOXConstants.NAMESPACE);
            soxResponse.setErrorDetails(errorDetails);
        }
        return soxResponse;
    }

    @PostMapping("/update-uar-data")
    public SOXResponse<Boolean> updateUserAccessReviewData(@RequestBody Map<Integer, String> request){
        String methodName = "updateUserAccessReviewData";
        SOXResponse<Boolean> soxResponse = new SOXResponse<>();
        try{
            Boolean success = legacyUARService.updateUserAccessReviewData(request);
            soxResponse.setData(success);
            soxResponse.setStatusCode(SOXConstants.STATUS_CODE_SUCCESS);
            soxResponse.setStatusMsg(SOXConstants.STATUS_MSG_SUCCESS);
        }catch (SOXException exception){
            ErrorDetails errorDetails = new ErrorDetails(exception.getErrorMsg(), exception.getStatusCode(), exception.getStatusMsg()
                    ,exception.getClassName(), exception.getMethodName(), SOXConstants.NAMESPACE);
            soxResponse.setErrorDetails(errorDetails);
        }catch (Exception exception){
            log.error(exception);
            ErrorDetails errorDetails = new ErrorDetails(exception.getMessage(), SOXConstants.STATUS_CODE_APPLICATION_ERROR, SOXConstants.STATUS_MSG_APPLICATION_ERROR
                    , className, methodName, SOXConstants.NAMESPACE);
            soxResponse.setErrorDetails(errorDetails);
        }
        return soxResponse;
    }

    @GetMapping("/report/{appId}/{status}")
    public SOXResponse<List<ApplicationUAREntity>> getUserAccessReviewDataReport(@PathVariable String appId, @PathVariable int status){
        String methodName = "getUserAccessReviewDataReport";
        SOXResponse<List<ApplicationUAREntity>> soxResponse = new SOXResponse<>();
        try{
            if(!(status == SOXConstants.UAR_STATUS_PENDING || status == SOXConstants.UAR_STATUS_HOLD ||
                    status ==SOXConstants.UAR_STATUS_COMPLETED || status == SOXConstants.UAR_STATUS_ALL)){
                throw  new SOXException("Invalid Status.", SOXConstants.STATUS_CODE_VALIDATION_ERROR, SOXConstants.STATUS_MSG_VALIDATION_ERROR
                            , className, methodName);
            }
            List<ApplicationUAREntity> entityList;
            if(status == SOXConstants.UAR_STATUS_ALL)
                entityList = legacyUARService.getUserAccessReviewDataForApp(appId);
            else
                entityList = legacyUARService.getUserAccessReviewData(appId, status);
            soxResponse.setData(entityList);
            soxResponse.setStatusCode(SOXConstants.STATUS_CODE_SUCCESS);
            soxResponse.setStatusMsg(SOXConstants.STATUS_MSG_SUCCESS);
        }catch (SOXException exception){
            ErrorDetails errorDetails = new ErrorDetails(exception.getErrorMsg(), exception.getStatusCode(), exception.getStatusMsg()
                    ,exception.getClassName(), exception.getMethodName(), SOXConstants.NAMESPACE);
            soxResponse.setErrorDetails(errorDetails);
        }catch (Exception exception){
            log.error(exception);
            ErrorDetails errorDetails = new ErrorDetails(exception.getMessage(), SOXConstants.STATUS_CODE_APPLICATION_ERROR, SOXConstants.STATUS_MSG_APPLICATION_ERROR
                    , className, methodName, SOXConstants.NAMESPACE);
            soxResponse.setErrorDetails(errorDetails);
        }
        return soxResponse;
    }
}
