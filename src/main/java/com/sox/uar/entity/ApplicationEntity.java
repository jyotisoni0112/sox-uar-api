package com.sox.uar.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "sox_application_details")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ApplicationEntity {

    @Id
    @Column(name = "app_id")
    private String appId;
    @Column(name = "app_name")
    private String appName;
    @Column(name = "active")
    private boolean active;
    @Column(name = "created_by")
    private String createdBy;
    @Column(name = "created_on")
    private Date createdOn;
    @Column(name = "updated_by")
    private String updatedBy;
    @Column(name = "updated_on")
    private Date updatedOn;
    @OneToOne
    @JoinColumn(name = "created_by", referencedColumnName = "user_id", insertable = false, updatable = false)
    private UserInfo createdByUser;
    @OneToOne
    @JoinColumn(name = "updated_by", referencedColumnName = "user_id", insertable = false, updatable = false)
    private UserInfo updatedByUser;

}
