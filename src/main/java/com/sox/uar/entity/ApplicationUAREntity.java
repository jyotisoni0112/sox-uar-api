package com.sox.uar.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

@Entity
@Table(name = "sox_application_uar")
@Data
public class ApplicationUAREntity {

    @Id
    @Column(name = "uar_key")
    private Integer primaryKey;
    @Column(name = "user_id")
    private String userId;
    @Column(name = "first_name")
    private String userFirstName;
    @Column(name = "last_name")
    private String userLastName;
    @Column(name = "user_email")
    private String userEmail;
    @Column(name = "app_id")
    private String appId;
    @Column(name = "loc_id")
    private String locId;
    @Column(name = "department")
    private String department;
    @Column(name = "data_center")
    private String dataCenter;
    @Column(name = "manager_user_id")
    private String managerUserId;
    @Column(name = "status")
    private int status;
    @Column(name = "access")
    private String access;
    @Column(name = "active")
    private boolean active;
    @Column(name = "created_by")
    private String createdBy;
    @Column(name = "created_on")
    private Date createdOn;
    @Column(name = "updated_by")
    private String updatedBy;
    @Column(name = "updated_on")
    private Date updatedOn;

}
