package com.sox.uar.payload;

import lombok.Data;

@Data
public class ApplicationRequest {

    private String appId;
    private String appName;
}
