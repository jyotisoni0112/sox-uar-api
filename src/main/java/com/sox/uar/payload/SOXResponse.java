package com.sox.uar.payload;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.sox.uar.utility.SOXConstants;
import com.sox.uar.vo.ErrorDetails;
import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class SOXResponse<T> {

    private String nameSpace = SOXConstants.NAMESPACE;
    private T data;
    private String statusCode = SOXConstants.STATUS_CODE_ERROR;
    private String statusMsg = SOXConstants.STATUS_MSG_ERROR;
    private ErrorDetails errorDetails;
    private String loggingId;

}
