package com.sox.uar.repository;

import com.sox.uar.entity.ApplicationEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ApplicationDetailsRepository extends JpaRepository<ApplicationEntity, String> {
}
