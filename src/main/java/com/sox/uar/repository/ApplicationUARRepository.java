package com.sox.uar.repository;

import com.sox.uar.entity.ApplicationUAREntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ApplicationUARRepository extends JpaRepository<ApplicationUAREntity, Integer> {

    List<ApplicationUAREntity> findByAppIdAndStatus(String appId, int status);
    List<ApplicationUAREntity> findByAppId(String appId);
    List<ApplicationUAREntity> findByManagerUserIdAndStatus(String appId, int status);
}
