package com.sox.uar.repository;

import com.sox.uar.entity.LocationEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface LocationDetailsRepository extends JpaRepository<LocationEntity, String> {
}
