package com.sox.uar.repository;

import com.sox.uar.entity.UserRoleDetails;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface UserRoleDetailsRepository extends JpaRepository<UserRoleDetails, Integer> {

    List<UserRoleDetails> findByUserId(String userId);
    List<UserRoleDetails> findByUserIdAndActive(String userId, boolean active);

}
