package com.sox.uar.security;

import com.sox.uar.entity.UserDetails;
import com.sox.uar.entity.UserRoleDetails;
import com.sox.uar.repository.UserDetailsRepository;
import com.sox.uar.repository.UserRoleDetailsRepository;
import com.sox.uar.utility.RoleUtility;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@Service
public class AuthService {

    @Autowired
    private UserRoleDetailsRepository userRoleDetailsRepository;

    @Autowired
    private UserDetailsRepository userDetailsRepository;

    public User getUserByUsername(String username){
        Set<GrantedAuthority> authorities = new HashSet<>();
        List<UserRoleDetails> roleDetailsList = userRoleDetailsRepository.findByUserIdAndActive(username, true);
        String userName;
        if(roleDetailsList.isEmpty()){
            Optional<UserDetails> userDetails = userDetailsRepository.findById(username);
            if(userDetails.isPresent()){
                userName = userDetails.get().getUserId();
            }else{
                throw new UsernameNotFoundException(username);
            }
        }else{
            roleDetailsList
                    .forEach(role -> authorities.add(new SimpleGrantedAuthority(RoleUtility.getRoleName(role.getRoleId()))));
            userName = roleDetailsList.get(0).getUserId();
        }

        return new User(userName,"password", authorities);
    }
}
