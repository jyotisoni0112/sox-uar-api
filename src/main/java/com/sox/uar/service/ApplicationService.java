package com.sox.uar.service;

import com.sox.uar.entity.ApplicationEntity;
import com.sox.uar.exception.SOXException;
import com.sox.uar.payload.ApplicationRequest;
import com.sox.uar.repository.ApplicationDetailsRepository;
import com.sox.uar.utility.SOXConstants;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
@Log4j2
public class ApplicationService {

    String className = ApplicationService.class.getName();

    @Autowired
    private ApplicationDetailsRepository applicationDetailsRepository;

    public List<ApplicationEntity> getAllApplicationDetails() throws SOXException {
        String methodName = "getAllApplicationDetails";
        List<ApplicationEntity> applicationEntityList;
        try{
            applicationEntityList = applicationDetailsRepository.findAll();
        }catch (Exception exception){
            log.error(exception);
            throw new SOXException("Exception while getting application details", SOXConstants.STATUS_CODE_APPLICATION_ERROR
                    , SOXConstants.STATUS_MSG_APPLICATION_ERROR, className, methodName);
        }
        return applicationEntityList;
    }

    public ApplicationEntity toggleApplicationStatus(String appId, String userId) throws SOXException {
        String methodName = "toggleApplicationStatus";
        ApplicationEntity toggledApplication;
        try{
            Optional<ApplicationEntity> applicationEntity = applicationDetailsRepository.findById(appId);
            if(applicationEntity.isPresent()){
                applicationEntity.get().setActive(!applicationEntity.get().isActive());
                applicationEntity.get().setUpdatedBy(userId.toLowerCase());
                applicationEntity.get().setUpdatedOn(new Date());
                toggledApplication = applicationDetailsRepository.save(applicationEntity.get());
            }else{
                throw new SOXException("Application not found with id "+appId, SOXConstants.STATUS_CODE_NOT_FOUND
                        , SOXConstants.STATUS_MSG_NOT_FOUND, className, methodName);
            }
        }catch (Exception exception){
            log.error(exception);
            throw new SOXException("Exception while toggling application status", SOXConstants.STATUS_CODE_APPLICATION_ERROR
                    , SOXConstants.STATUS_MSG_APPLICATION_ERROR, className, methodName);
        }
        return toggledApplication;
    }

    public ApplicationEntity addNewApplication(ApplicationRequest request, String userId) throws SOXException {
        String methodName = "addNewApplication";
        ApplicationEntity applicationEntity;
        try{
            ApplicationEntity application = new ApplicationEntity(request.getAppId(), request.getAppName(), true, userId, new Date()
                                    ,userId, new Date(), null, null);
            applicationEntity = applicationDetailsRepository.save(application);
        }catch (Exception exception){
            log.error(exception);
            throw new SOXException("Exception while adding new application.", SOXConstants.STATUS_CODE_APPLICATION_ERROR
                    , SOXConstants.STATUS_MSG_APPLICATION_ERROR, className, methodName);
        }
        return applicationEntity;
    }
}
