package com.sox.uar.service;

import com.sox.uar.entity.ApplicationUAREntity;
import com.sox.uar.exception.SOXException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public interface LegacyUARService {

    public List<ApplicationUAREntity> getUserAccessReviewData(String appId, int status) throws SOXException;

    public boolean updateUserAccessReviewData(Map<Integer, String> requestData) throws SOXException;

    public List<ApplicationUAREntity> getUserAccessReviewDataForApp(String appId) throws SOXException;
}
