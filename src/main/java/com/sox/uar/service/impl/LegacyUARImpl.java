package com.sox.uar.service.impl;

import com.sox.uar.entity.ApplicationUAREntity;
import com.sox.uar.entity.UserRoleDetails;
import com.sox.uar.exception.SOXException;
import com.sox.uar.repository.ApplicationUARRepository;
import com.sox.uar.repository.UserRoleDetailsRepository;
import com.sox.uar.service.LegacyUARService;
import com.sox.uar.utility.RoleUtility;
import com.sox.uar.utility.SOXConstants;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Log4j2
public class LegacyUARImpl implements LegacyUARService {

    private String className = LegacyUARImpl.class.getName();

    @Autowired
    private UserRoleDetailsRepository userRoleDetailsRepository;

    @Autowired
    private ApplicationUARRepository applicationUARRepository;

    public List<ApplicationUAREntity> getUserAccessReviewData(String appId, int status) throws SOXException {
        String methodName = "getUserAccessReviewData";
        List<ApplicationUAREntity> uarData;
        try{
            String userId = SecurityContextHolder.getContext().getAuthentication().getName();
            List<UserRoleDetails> roleDetails = userRoleDetailsRepository.findByUserIdAndActive(userId, true);
            List<UserRoleDetails> roleValidity = roleDetails.stream()
                    .filter(role -> (appId.equals(role.getAppId()) && (isRoleValidToReview(role.getRoleId())))
                            || role.getRoleId() == RoleUtility.ROLE_ADMIN)
                    .collect(Collectors.toList());
            if(roleValidity.isEmpty()){
                throw new SOXException("Access Denied.", SOXConstants.STATUS_CODE_UNAUTHORIZED, SOXConstants.STATUS_MSG_UNAUTHORIZED,
                        className, methodName);
            }else{
                uarData = applicationUARRepository.findByAppIdAndStatus(appId, status);
            }
        }catch (Exception exception){
            log.error(exception);
            throw new SOXException(exception.getMessage(), SOXConstants.STATUS_CODE_UNAUTHORIZED, SOXConstants.STATUS_MSG_UNAUTHORIZED,
                    className, methodName);
        }
        return uarData;
    }

    public boolean updateUserAccessReviewData(Map<Integer, String> requestData) throws SOXException {
        String methodName = "getUserAccessReviewData";
        try{
            String userId = SecurityContextHolder.getContext().getAuthentication().getName();
            List<UserRoleDetails> roleDetails = userRoleDetailsRepository.findByUserIdAndActive(userId, true);
            for(Integer key : requestData.keySet()){
                Optional<ApplicationUAREntity> entity = applicationUARRepository.findById(key);
                if(entity.isPresent()){
                    String appId = entity.get().getAppId();
                    List<UserRoleDetails> roleValidity = roleDetails.stream()
                            .filter(role -> (appId.equals(role.getAppId()) && (isRoleValidToReview(role.getRoleId())))
                                    || role.getRoleId() == RoleUtility.ROLE_ADMIN)
                            .collect(Collectors.toList());
                    if(roleValidity.isEmpty()){
                        throw new SOXException("Access Denied.", SOXConstants.STATUS_CODE_UNAUTHORIZED, SOXConstants.STATUS_MSG_UNAUTHORIZED,
                                className, methodName);
                    }else{
                        entity.get().setAccess(requestData.get(key));
                        entity.get().setUpdatedBy(userId);
                        entity.get().setUpdatedOn(new Date());
                        entity.get().setStatus(SOXConstants.UAR_STATUS_COMPLETED);
                        applicationUARRepository.save(entity.get());
                    }
                }

            }
        }catch (Exception exception){
            log.error(exception);
            throw new SOXException(exception.getMessage(), SOXConstants.STATUS_CODE_UNAUTHORIZED, SOXConstants.STATUS_MSG_UNAUTHORIZED,
                    className, methodName);
        }
        return true;
    }

    public List<ApplicationUAREntity> getUserAccessReviewDataForApp(String appId) throws SOXException {
        String methodName = "getUserAccessReviewData";
        List<ApplicationUAREntity> uarData;
        try{
            String userId = SecurityContextHolder.getContext().getAuthentication().getName();
            List<UserRoleDetails> roleDetails = userRoleDetailsRepository.findByUserIdAndActive(userId, true);
            List<UserRoleDetails> roleValidity = roleDetails.stream()
                    .filter(role -> (appId.equals(role.getAppId()) && (isRoleValidToReview(role.getRoleId())))
                            || role.getRoleId() == RoleUtility.ROLE_ADMIN)
                    .collect(Collectors.toList());
            if(roleValidity.isEmpty()){
                throw new SOXException("Access Denied.", SOXConstants.STATUS_CODE_UNAUTHORIZED, SOXConstants.STATUS_MSG_UNAUTHORIZED,
                        className, methodName);
            }else{
                uarData = applicationUARRepository.findByAppId(appId);
            }
        }catch (Exception exception){
            log.error(exception);
            throw new SOXException(exception.getMessage(), SOXConstants.STATUS_CODE_UNAUTHORIZED, SOXConstants.STATUS_MSG_UNAUTHORIZED,
                    className, methodName);
        }
        return uarData;
    }

    public static boolean isRoleValidToReview(int roleId){
        return roleId == RoleUtility.ROLE_BUSINESS_POC_1 || roleId == RoleUtility.ROLE_BUSINESS_POC_2 || roleId == RoleUtility.ROLE_BUSINESS_POC_3
                || roleId == RoleUtility.ROLE_BUSINESS_B_POC_1 || roleId == RoleUtility.ROLE_BUSINESS_B_POC_2 || roleId == RoleUtility.ROLE_BUSINESS_B_POC_3
                || roleId == RoleUtility.ROLE_ADMIN || roleId == RoleUtility.ROLE_READ_ONLY;
    }

}
