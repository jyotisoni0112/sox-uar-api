package com.sox.uar.utility;

public class UtilityMethod {

    public static String capitalize(String string) {
        if(string == null )
            return "";
        else{
            if(string.length()>1){
                return string.substring(0,1).toUpperCase()+string.substring(1);
            }else{
                return string.toUpperCase();
            }
        }
    }

    private UtilityMethod(){

    }

}
